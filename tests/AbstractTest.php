<?php


namespace App\Tests;


use App\Entity\Participant;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class AbstractTest extends WebTestCase
{

    protected function getUserBasic(){
        $kernel = self::bootKernel();
        $user = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Participant::class)
            ->findOneBy(array('pseudo'=>'user'));
        return $user;
    }
    protected function getUserAdmin(){
        $kernel = self::bootKernel();
        $user = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Participant::class)
            ->findOneBy(array('pseudo'=>'admin'));
        return $user;
    }
    protected function getUserOrga(){
        $kernel = self::bootKernel();
        $user = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Participant::class)
            ->findOneBy(array('pseudo'=>'orga'));
        return $user;
    }
    protected function setUser($user,$client){

        $container = $client->getContainer();
        $session = $container->get('session');
        $firewallName = 'main';
        $token = new UsernamePasswordToken($user,null,$firewallName,$user->getRoles());
        $session->set('_security_'.$firewallName,serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(),$session->getId());
        $client->getCookieJar()->set($cookie);
//
    }

}
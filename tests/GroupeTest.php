<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class GroupeTest extends AbstractTest
{

    public function testBasicGroupe(){

        $user = $this->getUserAdmin();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/groupe");
        $nbGroupe =  $crawler->filter('tr')->count();
        $this->assertGreaterThan(
            0,
            $crawler->filter('a[title="Ajouter un groupe"]')->count(),"Le bouton de rajout doit apparaitre"
        );
        $link = $crawler
            ->filter('a[title="Ajouter un groupe"]')
            ->link();
        $crawler = $client->click($link);

        $crawler = $client->request('GET', $link->getUri());
        $this->assertGreaterThan(
            0,
            $crawler->filter('input[name="groupe[nom]"]')->count(),"Le formulaire de création des groupes doit s'afficher"
        );


        $crawler = $client->click($link);
        $form = $crawler->selectButton('Enregistrer')->form();

        $crawler = $client->submitForm('Enregistrer', [
            'groupe[nom]' => 'JE FAIS UN TEST GROUPE',
        ]);
        $this->assertSame(302, $client->getResponse()->getStatusCode(),"Le bouton d'ajout doit fonctionner");
        $crawler = $client->followRedirect();
        $nbGroupeNew =  $crawler->filter('tr')->count();
        $this->assertEquals($nbGroupe+1,$nbGroupeNew,'Il doit y avoir une ligne en plus');



    }

}
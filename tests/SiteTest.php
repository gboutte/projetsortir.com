<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class SiteTest extends AbstractTest
{

    public function testBasicSite(){

        $user = $this->getUserAdmin();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/admin/site");
        $nbSites =  $crawler->filter('tr')->count();
        $this->assertGreaterThan(
            0,
            $crawler->filter('#sitesList>tr>td')->count(),"Il n'y a pas de site affiché alors qu'il y a bien une entrée en base"
        );
        $this->assertGreaterThan(
            0,
            $crawler->filter('#sitesList>tr>td>a[href="/admin/site/delete/1"]')->count(),"Le bouton de suppression doit apparaitre"
        );
        $link = $crawler
            ->filter('a[title="Ajouter un site"]')
            ->link();
        $crawler = $client->click($link);

        $crawler = $client->request('GET', $link->getUri());
        $this->assertGreaterThan(
            0,
            $crawler->filter('form[name="site"]')->count(),"Le formulaire de création des sites doit s'afficher"
        );
        $crawler = $client->click($link);
        $form = $crawler->selectButton('Enregistrer')->form();

        $crawler = $client->submitForm('Enregistrer', [
            'site[nom]' => 'JE FAIS UN TEST',
        ]);

        $this->assertSame(302, $client->getResponse()->getStatusCode(),"Le bouton d'ajout doit fonctionner");
        $crawler = $client->followRedirect();
        $nbSitesNew =  $crawler->filter('tr')->count();
        $this->assertEquals($nbSites+1,$nbSitesNew,'Il doit y avoir une ligne en plus');

    }

}
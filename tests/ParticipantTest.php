<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class ParticipantTest extends AbstractTest
{
    public function testAjoutSuppr(){
        $faker = \Faker\Factory::create();

        $user = $this->getUserAdmin();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/admin/participant/liste");

        $nbParticipant =  $crawler->filter('tbody > tr')->count();

        $buttonAdd = $crawler->filter('a[href="/admin/participant/nouveau"]');
        $this->assertGreaterThan(0,$buttonAdd->count(),"Il doit y avoir un bouton pour ajouter un participant");


        $buttonImport = $crawler->filter('a[href="/admin/participant/import"]');
        $this->assertGreaterThan(0,$buttonImport->count(),"Il doit y avoir un bouton pour importer des participants");


        //Test ajout
        $crawlerAjout = $client->click($buttonAdd->link());
        $this->assertSame(200, $client->getResponse()->getStatusCode(),"Le bouton d'ajout doit fonctionner");

        $form = $crawlerAjout->filter('input[type=submit]')->form();

        $username = $faker->userName;
        $form['registration_form[mail]'] = $faker->companyEmail;
        $form['registration_form[nom]'] = $faker->lastName;
        $form['registration_form[prenom]'] = $faker->lastName;
        $form['registration_form[telephone]'] = "0777777777";
        $form['registration_form[pseudo]'] = $username;
        $form['registration_form[plainPassword]'] = $faker->password;

        $crawlerAjoutValider = $client->submit($form);
        $this->assertSame(302, $client->getResponse()->getStatusCode(),"A la validation le formulaire doit rediriger vers la liste");
        $this->assertRegExp('/\/admin\/participant\/liste$/', $client->getResponse()->headers->get('location'),"A la validation le formulaire doit rediriger vers la liste");
        $crawlerAjoutValider = $client->followRedirect();
        $this->assertSame(200, $client->getResponse()->getStatusCode(),"A la validation le formulaire doit rediriger vers la liste");
        $newNbParticipant =  $crawlerAjoutValider->filter('tbody > tr')->count();

        $this->assertEquals($nbParticipant+1,$newNbParticipant,"Il doit y avoir un participant de plus");




        $lines = $crawlerAjoutValider->filter('tbody > tr');


        $userLines =$lines->each(function (Crawler $node, $i) {
            return array(
                'name' =>  $node->filter("td:nth-child(4)")->each(function (Crawler $name, $j) { return $name->text();})[0],
                'node'=>$node
            );
        });
        $userToDelete = null;
        foreach ($userLines as $u){

            if($u['name'] == $username){
                $userToDelete = $u['node'];
            }
        }

        $this->assertNotNull($userToDelete,"Il doit y avoir une ligne avec l'utilisateur");
        $urlToDelete = $userToDelete->filter('.delete-participant')->attr('data-url');

        $crawlerDeleted = $client->request('GET', $urlToDelete);

        $this->assertSame(302, $client->getResponse()->getStatusCode(),"A la suppression on doit rediriger vers la liste");
        $this->assertRegExp('/\/admin\/participant\/liste$/', $client->getResponse()->headers->get('location'),"A la suppression on doit rediriger vers la liste");

        $crawlerDeleted = $client->followRedirect();
        $this->assertSame(200, $client->getResponse()->getStatusCode(),"A la suppression on doit rediriger vers la liste");
        $nbParticipantAfterDelete =  $crawlerDeleted->filter('tbody > tr')->count();
        $this->assertEquals($newNbParticipant-1,$nbParticipantAfterDelete,"Il doit y avoir un participant en moins");



    }
    public function testShow(){

        $user = $this->getUserAdmin();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/admin/participant/liste");

        $lines = $crawler->filter('tbody > tr');


        $userLines =$lines->each(function (Crawler $node, $i) {
            return array(
                'node'=>$node
            );
        });
        $userToDelete = null;
        foreach ($userLines as $u){
            $u = $u['node'];
            $username = $u->filter("td:nth-child(4)")->each(function (Crawler $name, $j) { return $name->text();})[0];
            $showLink = $u->filter("td")->each(function (Crawler $td, $j) {
                if($td->filter(".fas.fa-search")->count() > 0){
                    return $td->filter("a")->each(function (Crawler $a, $j) {return $a->link();})[0];
                }
            });
            $showLink = array_filter($showLink);
            $showLink = array_shift($showLink);
            $showPagecrawler = $client->click($showLink);
            $usernameShowed = trim($showPagecrawler->filter('h1')->text());
            $this->assertSame(200, $client->getResponse()->getStatusCode(),"On doit pouvoir afficher un utilisateur");
            $this->assertRegExp('/.*'.$username.'.*/', $usernameShowed,"Le nom d'utilisateur n'est pas identique");

        }
        $crawler = $client->request('GET', "/participant/afficher/225545454454");
        $this->assertSame(404, $client->getResponse()->getStatusCode(),"Erreur 404 quand il existe pas");

      

    }
}
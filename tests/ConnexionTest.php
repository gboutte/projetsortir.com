<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class ConnexionTest extends AbstractTest
{

    public function testBasicSite(){

        $user = $this->getUserAdmin();
        $client = static::createClient();

        $crawler = $client->request('GET', "/login");
        $this->assertGreaterThan(
            0,
            $crawler->filter('button[type="submit"]')->count(),"Il faut que le formulaire de connexion s'affiche."
        );
        $this->assertEquals(
            1,
            $crawler->filter('a[href="/login"]')->count(),"Il faut que l'utilisateur ait la barre de navigation d'un non-authentifié"
        );



        $crawler = $client->request('GET', "/iForgotMyPassword");
        $this->assertGreaterThan(
            0,
            $crawler->filter('button[type="submit"]')->count(),"Il faut que le formulaire de mdp s'affiche."
        );

    }

}
<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class MailTest extends AbstractTest
{

    public function testBasicMail(){

        $client = static::createClient();

        // enables the profiler for the next request (it does nothing if the profiler is not available)
        $client->enableProfiler();
        $crawler = $client->request('POST', '/iForgotMyPassword');
        $client->enableProfiler();
        $form = $crawler->selectButton('Enregistrer')->form();

        $form['password_recover_form[mail]'] = 'test@live.fr';
        $crawler = $client->submit($form);
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        $this->assertSame(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

    }

}
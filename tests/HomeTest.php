<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class HomeTest extends AbstractTest
{

    public function testBasicSortie(){

        $user = $this->getUserBasic();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/");

        $tr = $this->trFromHome($crawler);
        $archive = false;

        foreach ($tr as $line){
            switch ($line['name']){
                case "ouverte":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertTrue($buttons['hasInscrire'],"L'utilisateur doit avoir le bouton s'inscrire");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie qui n'est pas organisée par lui");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si il n'est pas inscrit");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie deja ouverte");
//                    @todo test l'inscription
                    break;
                case "annuler":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie annuler.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie qui est deja annuler");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est annuler");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie annuler");
                    break;
                case "creer":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie non publiée.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie qui n'est pas organisée par lui");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie n'est pas publiée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui n'est pas organisée par lui ");
                    break;
                case "en cours":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie en cours.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie en cours");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est en cours");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est en cours ");

                    break;

                case "cloturee":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie cloturée.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie cloturée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est cloturée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est cloturée ");

                    break;
                case "passee":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie passée.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie passée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est passée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est passée ");

                    break;
                case "pas la":
                    $archive = true;
                    break;

            }
        }
        $this->assertFalse($archive,"Les sortie passée de plus de 30 jours ne doivent pas etre visible par defaut");

    }
    public function testAdminSortie(){

        $user = $this->getUserAdmin();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/");

        $tr = $this->trFromHome($crawler);

        $archive = false;
        foreach ($tr as $line){
            switch ($line['name']){
                case "ouverte":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertTrue($buttons['hasInscrire'],"L'utilisateur doit avoir le bouton s'inscrire");
                    $this->assertTrue($buttons['hasAnnuler'],"L'utilisateur admin doit pouvoir annuler une sortie ouverte ");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si il n'est pas inscrit");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie deja ouverte");
//                    @todo test l'inscription
                    break;
                case "annuler":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie annuler.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie qui est deja annuler");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est annuler");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie annuler");
                    break;
                case "creer":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie non publiée.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler n'est pas organisée par lui");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie n'est pas publiée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui n'est pas organisée par lui ");
                    break;
                case "en cours":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie en cours.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie en cours");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est en cours");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est en cours ");

                    break;
                case "cloturee":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie cloturée.");
                    $this->assertTrue($buttons['hasAnnuler'],"L'utilisateur doit pouvoir annuler une sortie cloturée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est cloturée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est cloturée ");

                    break;
                case "passee":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie passée.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie passée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est passée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est passée ");

                    break;
                case "pas la":
                    $archive = true;
                    break;

            }
        }
        $this->assertFalse($archive,"Les sortie passée de plus de 30 jours ne doivent pas etre visible par defaut");

    }
    public function testOrgaSortie(){

        $user = $this->getUserOrga();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/");

        $tr = $this->trFromHome($crawler);
        $archive = false;
        foreach ($tr as $line){
            switch ($line['name']){
                case "ouverte":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas avoir le bouton s'inscrire si il est l'organisateur");
                    $this->assertTrue($buttons['hasAnnuler'],"L'utilisateur doit pouvoir annuler une sortie qu'il organise ");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne peux pas se désister de sa sortie");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie deja ouverte");

                    break;
                case "annuler":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie annuler.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie qui est deja annuler");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est annuler");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie annuler");
                    break;
                case "creer":
                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas avoir le bouton s'inscrire si il est l'organisateur");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie non publiée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne peux pas se désister de sa sortie");
                    $this->assertTrue($buttons['hasPublier'],"L'utilisateur doit pouvoir publier sa sortie");
                    break;
                case "en cours":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie en cours.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie en cours");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est en cours");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est en cours ");

                    break;
                case "cloturee":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie cloturée.");
                    $this->assertTrue($buttons['hasAnnuler'],"L'utilisateur doit pouvoir annuler une sortie cloturée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est cloturée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est cloturée ");

                    break;
                case "passee":

                    $buttons = $this->buttonFromLine($line["node"]);
                    $this->assertFalse($buttons['hasInscrire'],"L'utilisateur ne doit pas pouvoir s'inscrire a une sortie passée.");
                    $this->assertFalse($buttons['hasAnnuler'],"L'utilisateur ne doit pas pouvoir annuler une sortie passée");
                    $this->assertFalse($buttons['hasDesister'],"L'utilisateur ne doit pas pouvoir se désister si la sortie est passée");
                    $this->assertFalse($buttons['hasPublier'],"L'utilisateur ne doit pas pouvoir publier une sortie qui est passée ");

                    break;
                case "pas la":
                    $archive = true;
                    break;


            }

        }
        $this->assertFalse($archive,"Les sortie passée de plus de 30 jours ne doivent pas etre visible par defaut");
    }
    private function trFromHome(Crawler $crawler){

        $tr = $crawler->filter("#sortiesList tr")->each(function (Crawler $node, $i) {
            return array(
                'name' =>  $node->filter("td:first-child")->each(function (Crawler $name, $j) { return $name->text();})[0],
                'node'=>$node
            );
        });
        return $tr;


    }
    private function buttonFromLine(Crawler $line){
        $hasDesister = false;
        $hasInscrire = false;
        $hasAnnuler = false;
        $hasPublier = false;
        $children = $line->filter("td:last-child")->each(function (Crawler $node, $i) {

            return $node->children();
        })[0];

        $classes = $children->each(function (Crawler $child, $i) {
            $class = $child->attr('class');
            return $class;

        });

        foreach ($classes as $class){
            if(preg_match("/desister/i",$class)){
                $hasDesister = true;
            }
            if(preg_match("/inscrire/i",$class)){
                $hasInscrire = true;
            }
            if(preg_match("/annuler/i",$class)){
                $hasAnnuler = true;
            }
            if(preg_match("/publier/i",$class)){
                $hasPublier = true;
            }

        }

        return array(
          "hasDesister"=>$hasDesister,
          "hasInscrire"=>$hasInscrire,
          "hasAnnuler"=>$hasAnnuler,
          "hasPublier"=>$hasPublier,
        );

    }

}
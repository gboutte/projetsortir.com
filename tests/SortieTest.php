<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 10/10/2019
 * Time: 14:54
 */

namespace App\Tests;

class SortieTest extends AbstractTest
{

    public function testAdd() {
        $faker = \Faker\Factory::create();

        $user = $this->getUserAdmin();
        $client = static::createClient();
        $this->setUser($user,$client);

        $crawler = $client->request('GET', "/");
        $nbSortie =  $crawler->filter('tbody > tr')->count();

        $crawler = $client->request('GET', "/sortie/add");

        $buttonAddLieu = $crawler->filter('#btnAddLieuModal');
        $this->assertGreaterThan(0,$buttonAddLieu->count(),"Il doit y avoir un bouton pour ajouter un lieu");

        $buttonAnnuler = $crawler->filter('a[href="/"]');
        $this->assertGreaterThan(0,$buttonAnnuler->count(),"Il doit y avoir un bouton pour annuler l'insertion et revenir à l'accueil");

        $buttonAdd = $crawler->filter('input[type=submit]');
        $this->assertGreaterThan(0,$buttonAdd->count(),"Il doit y avoir un bouton pour enregistrer et créer la sortie");

//        $form = $buttonAdd->form();
//
//        $form['sortie[nom]'] = $faker->text;
//        $form['sortie[date_debut]'] = $this->toString($faker->dateTime->format('d/m/Y H:i'));
//        $form['sortie[duree]'] = $faker->randomNumber(2);
//        $form['sortie[date_cloture]'] = $this->toString($faker->dateTime->format('d/m/Y H:i'));
//        $form['sortie[nb_inscription_max]'] = $faker->randomNumber(2);
//        $form['sortie[description]'] = $faker->text;
//        $value = $crawler->filter('#villes option:nth-child(2)')->attr('value');
//        $form['villes']->select($value);
//        $value = $crawler->filter('#lieux option:nth-child(2)')->attr('value');
//        $form['lieux']->select($value);
//
//        $crawlerAjoutValider = $client->submit($form);
//        var_dump($client->getResponse()->getStatusCode());
////        $this->assertSame(302, $client->getResponse()->getStatusCode(),"A la validation le formulaire doit rediriger vers la page d'accueil");
//        $crawlerAjoutValider = $client->followRedirect();
//        $this->assertEquals($nbSortie+1, $crawlerAjoutValider->filter('tbody > tr')->count());

    }

}

<?php

namespace App\Tests;

class RoutesTest extends AbstractTest
{
    private $userUrl = array(
        "/",
        "/sortie/add",
        "/groupe",
        "/participant/afficher/2",
        "/participant/mon-profil/infos",
        "/participant/mon-profil/mdp",
        "/participant/mon-profil/photo",
    );
    private $adminUrl = array(
        "/admin/participant/liste",
        "/admin/participant/nouveau",
        "/admin/participant/import",
        "/admin/site",
        "/admin/lieu",
        "/admin/ville"
    );


    public function testLoginNeeded(){


        $user =  $this->getUserBasic();

        foreach ($this->userUrl as $url){
            //Redirection vers login si pas connecté
            $client = static::createClient();
            $crawler = $client->request('GET', $url);
            $this->assertSame(302, $client->getResponse()->getStatusCode(),$url." doit rediriger vers login si l'utilisateur n'est pas connecté.");
            $this->assertRegExp('/\/login$/', $client->getResponse()->headers->get('location'),$url." doit rediriger vers login si l'utilisateur n'est pas connecté.");


            $client = static::createClient();
            $this->setUser($user,$client);

            $crawler = $client->request('GET', $url);
            $this->assertSame(200, $client->getResponse()->getStatusCode(),$url." doit etre accessible si l'utilisateur est connecté.");

        }

    }
    public function testLoginAdminNeeded(){


        $userBasic =  $this->getUserBasic();
        $userAdmin =  $this->getUserAdmin();

        foreach ($this->adminUrl as $url){
            //Redirection vers login si pas connecté
            $client = static::createClient();
            $crawler = $client->request('GET', $url);
            $this->assertSame(302, $client->getResponse()->getStatusCode(),$url." doit rediriger vers login si l'utilisateur n'est pas connecté.");
            $this->assertRegExp('/\/login$/', $client->getResponse()->headers->get('location'),$url." doit rediriger vers login si l'utilisateur n'est pas connecté.");

            //Redirection vers login si pas admin
            $client = static::createClient();
            $this->setUser($userBasic,$client);
            $crawler = $client->request('GET', $url);
            $this->assertSame(403, $client->getResponse()->getStatusCode(),$url." doit refuser si l'utilisateur n'est pas admin.");


            //Si admin = ok
            $client = static::createClient();
            $this->setUser($userAdmin,$client);

            $crawler = $client->request('GET', $url);
            $this->assertSame(200, $client->getResponse()->getStatusCode(),$url." doit etre accessible si l'utilisateur est connecté et admin.");

        }

    }
}

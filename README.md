# Sortir.com [![pipeline status](https://gitlab.com/gboutte/projetsortir.com/badges/master/pipeline.svg)](https://gitlab.com/gboutte/projetsortir.com/commits/master) [![coverage report](https://gitlab.com/gboutte/projetsortir.com/badges/master/coverage.svg)](https://gitlab.com/gboutte/projetsortir.com/commits/master)

## Installation
- Executer ces commandes
```sh 
composer install
```
```sh 
php bin/console doctrine:database:create
```
```sh 
php bin/console doctrine:migrations:migrate
```
- Créer un VirtualHost a partir de cet exemple
```apacheconf
<VirtualHost *:80>
    ServerName sortir

    DocumentRoot C:\wamp64\www\sortir.com\public
    <Directory C:\wamp64\www\sortir.com\public>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallbackResource /index.php
    </Directory>
</VirtualHost>
```

Exécuter les requêtes sql suivantes:
```sql
DROP PROCEDURE IF EXISTS inscriptions_cloturee;
DELIMITER |
CREATE PROCEDURE inscriptions_cloturee ()
BEGIN
        UPDATE sortie SET etat_id = 3 WHERE sortie.date_cloture < CURRENT_TIME AND sortie.etat_id = 2;
END|
DELIMITER ;
DROP PROCEDURE IF EXISTS sortie_passee;
DELIMITER |
CREATE PROCEDURE sortie_passee ()
BEGIN
        UPDATE sortie SET etat_id = 5 WHERE DATE_ADD(sortie.date_debut, INTERVAL sortie.duree DAY) < CURRENT_TIME AND sortie.etat_id = 4;
END|
DELIMITER ;
DROP PROCEDURE IF EXISTS sortie_en_cours;
DELIMITER |
CREATE PROCEDURE sortie_en_cours ()
BEGIN
        UPDATE sortie SET etat_id = 4 WHERE CURRENT_TIME BETWEEN sortie.date_debut AND DATE_ADD(sortie.date_debut, INTERVAL sortie.duree DAY) AND (sortie.etat_id = 2 OR sortie.etat_id = 3);
END|
DELIMITER ;
DROP PROCEDURE IF EXISTS change_etat_sortie;
DELIMITER |
CREATE PROCEDURE change_etat_sortie ()
BEGIN
        CALL inscriptions_cloturee();
        CALL sortie_en_cours();
        CALL sortie_passee();
END|
DELIMITER ;
SET GLOBAL event_scheduler = 'ON';
DROP EVENT IF EXISTS refresh_etat_sortie;
CREATE EVENT refresh_etat_sortie
    ON SCHEDULE EVERY 1 MINUTE
    DO
      CALL change_etat_sortie();
```

### Utilisateur admin par défaut
Email
> test@live.fr

Mot de passe
> admin123

## Fonctionnalités
- Participant
    - Se connecter
        - Se souvenir de moi
    - Gérer son profil
        - Informations
        - Mot de passe
        - Photo de profil
    - Afficher le profil des autres participants
    - Inscrir les participant
        - Manuellement
        - Par fichier CSV / JSON
    - Désactivé un participant
    - Supprimer un participant
    - Mot de passe oublié
- Gestion des villes
- Sortie
    - Afficher la liste des sorties
    - Créer une sortie
    - S'inscrir a une sortie
    - Se désister
    - Annuler une sortie
    - Filtrer les sorties
    - Archiver les sortie
- Groupes privés
- Grstion des lieux


## Tests

Il faut créer un fichier .env.test.local

```dotenv
# define your env variables for the test env here
KERNEL_CLASS='App\Kernel'
APP_SECRET='$ecretf0rt3st'
SYMFONY_DEPRECATIONS_HELPER=999999
PANTHER_APP_ENV=panther
DATABASE_URL=mysql://<user>:<password>@127.0.0.1:3306/<dbtest>
```

Puis pour lancer les tests
```shell script
composer test
```




<?php


namespace App\Utils;


use App\Entity\Participant;
use App\Entity\ParticipantHash;
use App\Entity\Site;
use App\Repository\ParticipantHashRepository;
use App\Repository\ParticipantRepository;
use DateTime;
use Ramsey\Uuid\Uuid;

class ParticipantUtils
{
    public static function findInArray($array,$cols){
        $value = null;
        $i =0;
        while ($value == null && $i < count($cols)){
            if(!empty($array[$cols[$i]])){
                $value = $array[$cols[$i]];
            }
            $i++;
        }
        return $value;
    }


    public static function csvToArray($file){
        $columns = array();
        $values = array();

        $isOk = false;
        $row = 0;
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);

                $row++;
                if(empty($columns)){
                    for ($c=0; $c < $num; $c++) {

                        $columns[$c] = str_replace(array(' ','_'),'',strtolower($data[$c]));

                    }
                }else{
                    for ($c=0; $c < $num; $c++) {

                        $values[$row][$columns[$c]] = $data[$c];
                    }
                }

                if(!$isOk){
                    foreach ($columns as $col){
                        if(!empty($col)){
                            $isOk = true;
                        }
                    }
                    if(!$isOk){
                        $columns = array();
                    }
                }
            }
            fclose($handle);
        }

        return $values;
    }
    public static function arrayToParticipant($array,$em,$encoder = null){

        $newUser = new Participant();


        $mail =  ParticipantUtils::findInArray($array,array('email','mail'));
        if(!empty($mail)){
            $newUser->setMail($mail);
        }
        $pseudo =  ParticipantUtils::findInArray($array,array('pseudo','username'));
        if(!empty($pseudo)){
            $newUser->setPseudo($pseudo);
        }
        $nom =  ParticipantUtils::findInArray($array,array('nom'));
        if(!empty($nom)){
            $newUser->setNom($nom);
        }
        $prenom =  ParticipantUtils::findInArray($array,array('prenom','prénom'));
        if(!empty($prenom)){
            $newUser->setPrenom($prenom);
        }
        $telephone =  ParticipantUtils::findInArray($array,array('tel','telephone'));
        if(!empty($prenom)){
            $newUser->setTelephone($telephone);
        }
        $mdp =  ParticipantUtils::findInArray($array,
            array(
                'motdepasse',
                'mdp',
                'password',
                'passwd',
            ));
        if(!empty($mdp)){
            if(empty($encoder)){
                $newUser->setMotDePasse(password_hash($mdp,PASSWORD_BCRYPT));
            }else{
                $newUser->setMotDePasse(
                    $encoder->encodePassword(                                                           // but not an administrator
                        $newUser,
                        $mdp
                    )

                );

            }

        }

        $admin =  ParticipantUtils::findInArray($array,array('admin'));
        if(!empty($admin)){
            $newUser->setAdmin(boolval($admin));
        }else{
            $newUser->setAdmin(false);
        }
        $actif =  ParticipantUtils::findInArray($array,array('actif'));
        if(!empty($actif)){
            $newUser->setActif(boolval($actif));
        }else{
            $newUser->setActif(false);
        }
        $photo =  ParticipantUtils::findInArray($array,array('photourl','photo'));
        if(!empty($photo)){
            $newUser->setPhotoUrl($photo);
        }
        $site =  ParticipantUtils::findInArray($array,array('site'));
        if(!empty($site)){
            $siteObj = $em->getRepository(Site::class)->findOneBy(array('nom'=>$site));
            if(empty($siteObj)){
                $siteObj =  new Site();
                $siteObj->setNom($site);
                $em->persist($siteObj);
                $em->flush();
                $newUser->setSite($siteObj);
            }else{
                $newUser->setSite($siteObj);
            }
            $newUser->setPhotoUrl($photo);
        }
        return $newUser;
    }

    /**
     * generate a hash and added it into the database, usefull to create unique url
     * when a hash is generated, the older one is deleted by the participant id
     * @param Participant $participant
     * @param ParticipantRepository $repo
     * @param ParticipantHashRepository $repoHash
     * @return ParticipantHash
     */
    public static function generateParticipantHash(Participant $participant, ParticipantRepository $repo, ParticipantHashRepository $repoHash, $em) {
            $uuid1 = Uuid::uuid1();
            $ancienshash = $repoHash->findBy(array('id_participant' => $participant->getId()));                         // getting the old hashes
            if($ancienshash) {                                                                                          // deleting them
                foreach($ancienshash as $element) {
                    $em->remove($element);
                }
                $em->flush();
            }
            $hash = new ParticipantHash();
            $hash->setHash($uuid1);
            $hash->setIdParticipant($participant);
            $hash->setDate(new DateTime(date('Y-m-d H:i:s')));
            $em->persist($hash);
            $em->flush();
            return $hash;
    }




}
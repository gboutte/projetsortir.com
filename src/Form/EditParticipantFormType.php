<?php

namespace App\Form;

use App\Entity\Participant;
use App\Entity\Site;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\IsTrue;

class EditParticipantFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $basicSize = ['maxlength' => '40','minlength' => '3'];
        $builder
            ->add('nom',TextType::class,['label' => 'Nom', 'attr' => $basicSize])
            ->add('prenom',TextType::class,['label' => 'Prenom', 'attr' => $basicSize])
            ->add('telephone',TelType::class,['label' => 'Téléphone', 'attr' => ['maxlength' => '20','minlength' => '9','class'=>"input-phone"]])
            ->add('pseudo',TextType::class,['label' => 'Pseudo', 'attr' => $basicSize])
            ->add('mail',EmailType::class,['label' => 'Email', 'attr' => $basicSize])
            ->add('site', EntityType::class, [      // single select shot
                'class' => Site::class,
                'choice_label' => 'nom',
                'expanded' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participant::class,
        ]);
    }
}

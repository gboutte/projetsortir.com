<?php

namespace App\Form;

use App\Entity\Groupe;
use App\Entity\Sortie;
use App\Repository\GroupeRepository;
use App\Repository\ParticipantRepository;
use App\Repository\SortieRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;

class SortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, ['label' => 'Nom de la sortie'])
            ->add('date_debut', DateType::class,
                [
                    'label' => 'Date et heure de la sortie',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => ['class' => 'js-datepicker dateTimePicker']])
            ->add('duree', IntegerType::class, ['label' => 'Durée'])
            ->add('date_cloture', DateType::class, ['label' => 'Date limite d\'inscription', 'format' => 'dd/MM/yyyy HH:mm','widget' => 'single_text', 'html5' => false, 'attr' => ['class' => 'js-datepicker dateTimePicker']])
            ->add('nb_inscription_max', IntegerType::class, ['label' => 'Nombre de places'])
            ->add('description', TextareaType::class, ['label' => 'Description et infos', 'required' => false])
            ->add('villeOrga', TextType::class, ['mapped' => false, 'label' => 'Ville organisatrice', 'disabled' => true, 'required' => false])
//            ->add('ville', ChoiceType::class, ['mapped' => false, 'label' => 'Ville'])
//          ->add('lieu', ChoiceType::class, ['mapped' => false, 'label' => 'Lieu'])
            ->add('rue', TextType::class, ['mapped' => false, 'label' => 'Rue', 'disabled' => true, 'required' => true])
            ->add('latitude', NumberType::class, ['mapped' => false, 'label' => 'Latitude', 'disabled' => true, 'required' => false])
            ->add('longitude', NumberType::class, ['mapped' => false, 'label' => 'Longitude', 'disabled' => true, 'required' => false])
            ->add('groupeOrganisateur', EntityType::class, [      // single select shot
                'class' => Groupe::class,
                'choice_label' => 'nom',
                'expanded' => false,
                'choices' => $options['groupes'],
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
            'groupes' => Array()
        ]);
        $resolver->setDefined('groupes');
    }
}

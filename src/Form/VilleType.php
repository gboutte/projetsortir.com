<?php

namespace App\Form;

use App\Entity\Ville;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class VilleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,['label' => 'Nom', 'constraints' => [ new NotBlank(['message' => 'Veuillez saisir un nom'])]])
            ->add('code_postal', TextType::class, ['label' => 'Code postal',
                'attr' => ['minlength' => '5', 'maxlength' => '5'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un code postal',
                    ]),
                    new Length([
                        'min' => 5,
                        'minMessage' => 'Le code postal doit faire {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 5,
                        'maxMessage' => 'Le code postal doit faire {{ limit }} caractères',
                    ]),
                ],])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ville::class,
        ]);
    }
}

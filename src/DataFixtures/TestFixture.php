<?php

namespace App\DataFixtures;

use App\Entity\Etat;
use App\Entity\Lieu;
use App\Entity\Participant;
use App\Entity\Site;
use App\Entity\Sortie;
use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TestFixture extends Fixture
{
    private $userOrga;
    public function load(ObjectManager $manager)
    {

        $this->loadUsers($manager);
        $this->loadSorties($manager);

    }

    private function loadUsers(ObjectManager $manager){
        $faker = \Faker\Factory::create();

        $site = new Site();
        $site->setNom($faker->words(1,true));

        $manager->persist($site);

        $basicUser = new Participant();
        $basicUser->setActif(1);
        $basicUser->setAdmin(0);
        $basicUser->setNom($faker->lastName);
        $basicUser->setPrenom($faker->firstName);
        $basicUser->setPseudo("user");
        $basicUser->setSite($site);
        $basicUser->setMail($faker->email);
        $basicUser->setTelephone('0666666666');
        $basicUser->setMotDePasse(
            password_hash("123",PASSWORD_BCRYPT)
        );

        $manager->persist($basicUser);

        $adminUser = new Participant();
        $adminUser->setActif(1);
        $adminUser->setAdmin(1);
        $adminUser->setNom($faker->lastName);
        $adminUser->setPrenom($faker->firstName);
        $adminUser->setPseudo("admin");
        $adminUser->setSite($site);
        $adminUser->setMail($faker->email);
        $adminUser->setTelephone('0666666666');
        $adminUser->setMotDePasse(
            password_hash("123",PASSWORD_BCRYPT)
        );
        $manager->persist($adminUser);
        
        $orgaUser = new Participant();
        $orgaUser->setActif(1);
        $orgaUser->setAdmin(0);
        $orgaUser->setNom($faker->lastName);
        $orgaUser->setPrenom($faker->firstName);
        $orgaUser->setPseudo("orga");
        $orgaUser->setSite($site);
        $orgaUser->setMail($faker->email);
        $orgaUser->setTelephone('0666666666');
        $orgaUser->setMotDePasse(
            password_hash("123",PASSWORD_BCRYPT)
        );
        $manager->persist($orgaUser);
        $this->userOrga = $orgaUser;

        $manager->flush();
    }

    private function loadSorties(ObjectManager $manager){
        $faker = \Faker\Factory::create();

        $ville = new Ville();
        $ville->setNom($faker->city);
        $ville->setCodePostal($faker->postcode);

        $manager->persist($ville);

        $lieu = new Lieu();
        $lieu->setNom($faker->words(1,true));
        $lieu->setRue($faker->word);
        $lieu->setVille($ville);
        $lieu->setLatitude($faker->latitude);
        $lieu->setLongitude($faker->longitude);
        $manager->persist($lieu);


        $dateDebut = new \DateTime();
        $dateDebut->add(new \DateInterval("P1D"));
        $dateCloture = new \DateTime();
        $dateCloture->add(new \DateInterval("P6D"));
        $etats = $manager->getRepository(Etat::class)->findAll();

        $sortieCreer = $this->newSortie(
            "creer",
            $dateDebut,
            $this->userOrga,
            $lieu,
            6,
            $dateCloture,
            10,
            $faker->words(5,true),
            false,
            $etats[0]
        );
        $manager->persist($sortieCreer);

        $sortieOuverte = $this->newSortie(
            "ouverte",
            $dateDebut,
            $this->userOrga,
            $lieu,
            6,
            $dateCloture,
            10,
            $faker->words(5,true),
            true,
            $etats[1]
        );
        $manager->persist($sortieOuverte);


        $sortieAnnuler = $this->newSortie(
            "annuler",
            $dateDebut,
            $this->userOrga,
            $lieu,
            6,
            $dateCloture,
            10,
            $faker->words(5,true),
            true,
            $etats[5]
        );
        $manager->persist($sortieAnnuler);


        $sortieEnCours = $this->newSortie(
            "en cours",
            new \DateTime(),
            $this->userOrga,
            $lieu,
            6,
            $dateCloture,
            10,
            $faker->words(5,true),
            true,
            $etats[3]
        );
        $manager->persist($sortieEnCours);
        $sortieCloturee = $this->newSortie(
            "cloturee",
            $dateDebut,
            $this->userOrga,
            $lieu,
            6,
            new \DateTime(),
            10,
            $faker->words(5,true),
            true,
            $etats[2]
        );
        $manager->persist($sortieCloturee);


        $sortieCloturee = $this->newSortie(
            "pas la",
            new \DateTime('2000-01-01 00:00:00'),
            $this->userOrga,
            $lieu,
            6,
            new \DateTime('2000-01-01 00:00:00'),
            10,
            $faker->words(5,true),
            true,
            $etats[4]
        );
        $manager->persist($sortieCloturee);


        $dateDebut = new \DateTime();
        $dateDebut->sub(new \DateInterval("P1D"));
        $sortiePassee = $this->newSortie(
            "passee",
            $dateDebut,
            $this->userOrga,
            $lieu,
            1,
            $dateDebut,
            10,
            $faker->words(5,true),
            true,
            $etats[4]
        );
        $manager->persist($sortiePassee);


        $manager->persist(($this->newSite('test1')));

        $manager->flush();
    }

    private function newSortie($nom,$debut,$orga,$lieu,$duree,$cloture,$nbmax,$desc,$publie,$etat){
        $sortie = new Sortie();
        $sortie->setNom($nom);
        $sortie->setDateDebut($debut);
        $sortie->setOrganisateur($orga);
        $sortie->setLieu($lieu);
        $sortie->setDuree($duree);
        $sortie->setDateCloture($cloture);
        $sortie->setNbInscriptionMax($nbmax);
        $sortie->setDescription($desc);
        $sortie->setPublie($publie);
        $sortie->setEtat($etat);
        return $sortie;
    }

    private function newSite($nom){
        $site = new Site();
        $site->setNom($nom);
        return $site;
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191008082620 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE participant_hash (id INT AUTO_INCREMENT NOT NULL, id_participant_id INT NOT NULL, date DATETIME NOT NULL, hash VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_50C7086A07A8D1F (id_participant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe (id INT AUTO_INCREMENT NOT NULL, proprietaire_groupe_id INT NOT NULL, nom VARCHAR(100) NOT NULL, INDEX IDX_4B98C211091B44E (proprietaire_groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE participant_hash ADD CONSTRAINT FK_50C7086A07A8D1F FOREIGN KEY (id_participant_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C211091B44E FOREIGN KEY (proprietaire_groupe_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE participant CHANGE telephone telephone VARCHAR(15) DEFAULT NULL, CHANGE photo_url photo_url VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sortie CHANGE duree duree INT DEFAULT NULL, CHANGE description description VARCHAR(500) DEFAULT NULL, CHANGE etat_sortie etat_sortie INT DEFAULT NULL, CHANGE url_photo url_photo VARCHAR(250) DEFAULT NULL, CHANGE motif motif VARCHAR(500) DEFAULT NULL');
        $this->addSql('ALTER TABLE lieu CHANGE ville_id ville_id INT DEFAULT NULL, CHANGE rue rue VARCHAR(30) DEFAULT NULL, CHANGE latitude latitude DOUBLE PRECISION DEFAULT NULL, CHANGE longitude longitude DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE participant_hash');
        $this->addSql('DROP TABLE groupe');
        $this->addSql('ALTER TABLE lieu CHANGE ville_id ville_id INT DEFAULT NULL, CHANGE rue rue VARCHAR(30) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE latitude latitude DOUBLE PRECISION DEFAULT \'NULL\', CHANGE longitude longitude DOUBLE PRECISION DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE participant CHANGE telephone telephone VARCHAR(15) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE photo_url photo_url VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE sortie CHANGE duree duree INT DEFAULT NULL, CHANGE description description VARCHAR(500) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE etat_sortie etat_sortie INT DEFAULT NULL, CHANGE url_photo url_photo VARCHAR(250) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE motif motif VARCHAR(500) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}

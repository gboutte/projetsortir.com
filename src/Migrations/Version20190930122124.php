<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190930122124 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE participant ADD pseudo VARCHAR(50) NOT NULL, ADD photo_url VARCHAR(255) DEFAULT NULL, CHANGE telephone telephone VARCHAR(15) DEFAULT NULL, CHANGE mot_de_passe mot_de_passe VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE sortie CHANGE duree duree INT DEFAULT NULL, CHANGE description description VARCHAR(500) DEFAULT NULL, CHANGE etat_sortie etat_sortie INT DEFAULT NULL, CHANGE url_photo url_photo VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE lieu CHANGE ville_id ville_id INT DEFAULT NULL, CHANGE rue rue VARCHAR(30) DEFAULT NULL, CHANGE latitude latitude DOUBLE PRECISION DEFAULT NULL, CHANGE longidude longidude DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lieu CHANGE ville_id ville_id INT DEFAULT NULL, CHANGE rue rue VARCHAR(30) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE latitude latitude DOUBLE PRECISION DEFAULT \'NULL\', CHANGE longidude longidude DOUBLE PRECISION DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE participant DROP pseudo, DROP photo_url, CHANGE telephone telephone VARCHAR(15) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE mot_de_passe mot_de_passe VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE sortie CHANGE duree duree INT DEFAULT NULL, CHANGE description description VARCHAR(500) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE etat_sortie etat_sortie INT DEFAULT NULL, CHANGE url_photo url_photo VARCHAR(250) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}

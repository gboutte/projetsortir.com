<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191002125109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sortie ADD motif VARCHAR(500) DEFAULT NULL');
        $this->addSql("INSERT INTO site (nom) VALUES ('Saint herblain')");
        $this->addSql("INSERT INTO `participant` (`id`, `site_id`, `nom`, `prenom`, `telephone`, `mail`, `mot_de_passe`, `admin`, `actif`, `pseudo`, `photo_url`) VALUES
            (1, 1, 'boby', 'bob', '0515452535', 'test@live.fr', '$2y$13\$jjhtNTM9zu2giDXyNhfGXeZGHBKO2eLz9Sgi2D3qHin3l8nvFdSgC', 1, 1, 'bobobobob', NULL)");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sortie DROP motif');
    }
}

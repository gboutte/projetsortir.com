<?php

namespace App\Entity;

use App\Repository\ParticipantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParticipantRepository")
 * @UniqueEntity(fields={"mail"}, message="There is already an account with this mail")
 */
class Participant implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\Length(
     *      min = 2,
     *      max = 30,
     *      minMessage = "Minimum : {{ limit }} characters long",
     *      maxMessage = "Maximum :  {{ limit }} characters"
     * )
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\Length(
     *      min = 3,
     *      max = 30,
     *      minMessage = "Minimum : {{ limit }} characters long",
     *      maxMessage = "Maximum :  {{ limit }} characters"
     * )
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(
     *      min = 9,
     *      max = 15,
     *      minMessage = "Minimum : {{ limit }} characters long",
     *      maxMessage = "Maximum :  {{ limit }} characters"
     * )
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      min = 8,
     *      max = 100,
     *      minMessage = "Minimum : {{ limit }} characters long",
     *      maxMessage = "Maximum :  {{ limit }} characters"
     * )
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mot_de_passe;

    /**
     * @ORM\Column(type="boolean")
     */
    private $admin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Site", inversedBy="participants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $site;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sortie", mappedBy="organisateur")
     */
    private $ses_sorties;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sortie", mappedBy="inscrits")
     */
    private $sorties;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo_url;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Groupe", mappedBy="proprietaireGroupe", orphanRemoval=true)
     */
    private $groupes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Groupe", mappedBy="participants")
     */
    private $groupesPresence;

    public function __construct()
    {
        $this->setAdmin(false);
        $this->ses_sorties = new ArrayCollection();
        $this->sorties = new ArrayCollection();
        $this->groupes = new ArrayCollection();
        $this->groupesPresence = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = str_replace(" ","", $telephone);

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = strtolower($mail);

        return $this;
    }

    public function getMotDePasse(): ?string
    {
        return $this->mot_de_passe;
    }

    public function setMotDePasse(string $mot_de_passe): self
    {
        $this->mot_de_passe = $mot_de_passe;

        return $this;
    }

    public function getAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return Collection|Sortie[]
     */
    public function getSesSorties(): Collection
    {
        return $this->ses_sorties;
    }

    public function addSesSorty(Sortie $sesSorty): self
    {
        if (!$this->ses_sorties->contains($sesSorty)) {
            $this->ses_sorties[] = $sesSorty;
            $sesSorty->setOrganisateur($this);
        }

        return $this;
    }

    public function removeSesSorty(Sortie $sesSorty): self
    {
        if ($this->ses_sorties->contains($sesSorty)) {
            $this->ses_sorties->removeElement($sesSorty);
            // set the owning side to null (unless already changed)
            if ($sesSorty->getOrganisateur() === $this) {
                $sesSorty->setOrganisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sortie[]
     */
    public function getSorties(): Collection
    {
        return $this->sorties;
    }

    public function addSorty(Sortie $sorty): self
    {
        if (!$this->sorties->contains($sorty)) {
            $this->sorties[] = $sorty;
            $sorty->addInscrit($this);
        }

        return $this;
    }

    public function removeSorty(Sortie $sorty): self
    {
        if ($this->sorties->contains($sorty)) {
            $this->sorties->removeElement($sorty);
            $sorty->removeInscrit($this);
        }

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPhotoUrl(): ?string
    {
        return $this->photo_url;
    }

    public function setPhotoUrl(?string $photo_url): self
    {
        $this->photo_url = $photo_url;

        return $this;
    }
    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @param ParticipantRepository $userFactory
     * @return Participant
     */
    public function loadUserByUsername($username, ParticipantRepository $userFactory)
    {
        return($userFactory->findOneBy(array('mail'=>$username)));
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }

    /**
     * Returns the roles granted to the user.
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        if($this->getAdmin()){
            $roles = ['ROLE_ADMIN'];
        }else{
            $roles = ['ROLE_USER'];
        }
        return $roles;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string|null The encoded password if any
     */
    public function getPassword()
    {
        return $this->getMotDePasse();
    }


    public function setPassword($mot_de_passe)
    {
        $this->setMotDePasse($mot_de_passe);
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getMail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
            $groupe->setProprietaireGroupe($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self
    {
        if ($this->groupes->contains($groupe)) {
            $this->groupes->removeElement($groupe);
            // set the owning side to null (unless already changed)
            if ($groupe->getProprietaireGroupe() === $this) {
                $groupe->setProprietaireGroupe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupesPresence(): Collection
    {
        return $this->groupesPresence;
    }

    public function addGroupesPresence(Groupe $groupesPresence): self
    {
        if (!$this->groupesPresence->contains($groupesPresence)) {
            $this->groupesPresence[] = $groupesPresence;
            $groupesPresence->addParticipant($this);
        }

        return $this;
    }

    public function removeGroupesPresence(Groupe $groupesPresence): self
    {
        if ($this->groupesPresence->contains($groupesPresence)) {
            $this->groupesPresence->removeElement($groupesPresence);
            $groupesPresence->removeParticipant($this);
        }

        return $this;
    }
}

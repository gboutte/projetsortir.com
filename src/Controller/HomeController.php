<?php

namespace App\Controller;

use App\Entity\Groupe;
use App\Repository\GroupeRepository;
use App\Repository\ParticipantRepository;
use App\Repository\SiteRepository;
use App\Repository\SortieRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param SortieRepository $repo
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SortieRepository $repo, SiteRepository $repoSite, Request $request, ParticipantRepository $repoParticipant, GroupeRepository $groupeRepository,EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $filtres['site'] = $request->get('site');
        $filtres['sortie'] = $request->get('sortie');
        $filtres['deb'] = $request->get('deb');
        $filtres['fin'] = $request->get('fin');
        $filtres['organisateur'] = $request->get('organisateur');
        $filtres['inscrit'] = $request->get('inscrit');
        $filtres['notInscrit'] = $request->get('notInscrit');
        $filtres['passee'] = $request->get('passee');
        $filtres['id'] = $this->getUser()->getId();
        $filtres['groupeOrganisateur'] = $groupeRepository->findByParticipantsGroupe($em,$repoParticipant,$user);
        $sorties = $repo->findByFilterFields($filtres, $repoParticipant);
        $sites = $repoSite->findAll();

        return $this->render('home/index.html.twig', [
            'sorties' => $sorties,
            'sites' => $sites,
            'filtres' => $filtres
        ]);
    }
}

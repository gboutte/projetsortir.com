<?php

namespace App\Controller;

use App\Repository\VilleRepository;
use App\Entity\Ville;
use App\Form\VilleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Annotations\AnnotationReader;

class VilleController extends AbstractController
{
    /**
     * @Route("/admin/ville", name="ville")
     */
    public function index(VilleRepository $repo, EntityManagerInterface $em, Request $request)
    {
        $filtres['ville'] = $request->get('ville');
        $villes = $repo->findByfilterFields($filtres, $repo);

        return $this->render("ville/index.html.twig",
            [
                "villes" => $villes,
                "filtres" => $filtres,
            ]);
    }

    /**
     * @Route("/admin/ville/add", name="ville_add")
     */
    public function add(Request $request, EntityManagerInterface $em)
    {
        $ville = new Ville();
        //Creation du formulaire avec un objet vide
        $form = $this->createForm(VilleType::class, $ville);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            //Insert
            $em->persist($ville);
            $em->flush();

            return $this->redirectToRoute("ville", ["id" => $ville->getId()]);
        }

        return $this->render("ville/add.html.twig", ["villeForm" => $form->createView()]);
    }

    /**
     * @Route("/admin/ville/update/{id}", name="ville_update", requirements={"id":"\d+"})
     */
    public function update(Ville $ville, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(VilleType::class,$ville);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($ville);
            $em->flush();

            $this->addFlash('success', 'Ville successfully updated!');
            return $this->redirectToRoute('ville');
        }

        return $this->render("ville/add.html.twig", ["villeForm" => $form->createView()]);
    }

    /**
     * @Route("/admin/ville/del/{id}", name="ville_del")
     */
    public function del(Ville $ville, EntityManagerInterface $em){
            $em->remove($ville);
            $em->flush();
            $this->addFlash("success", "Ville successfully deleted!");

        return $this->redirectToRoute('ville');
    }
}


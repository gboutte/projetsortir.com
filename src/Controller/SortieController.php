<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Form\SortieType;
use App\Repository\EtatRepository;
use App\Repository\GroupeRepository;
use App\Repository\LieuRepository;
use App\Repository\ParticipantRepository;
use App\Repository\SiteRepository;
use App\Repository\SortieRepository;
use App\Repository\VilleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class SortieController extends AbstractController
{
    /**
     * @Route("/sortie/add", name="add_sortie")
     */
    public function add(Request $request, EntityManagerInterface $em, VilleRepository $repoVille, ParticipantRepository $repoParticipant,
                        LieuRepository $repoLieu, SiteRepository $repoSite, EtatRepository $repoEtat, GroupeRepository $groupeRepository)
    {
        $user = $this->getUser();
        $organisateur = $repoParticipant->findOneBy(['id' => $user->getId()]);
        $villes = $repoVille->findAll();
        $lieux = $repoLieu->findAll();
        $site = $repoSite->findOneBy(['id' => $organisateur->getSite()->getId()]);
        $groupeUser = $groupeRepository->findBy(Array('proprietaireGroupe'=>$user->getId()));
        $sortie = new Sortie();
        $sortieForm = $this->createForm(SortieType::class, $sortie,Array('groupes'=>$groupeUser));
        $sortieForm->handleRequest($request);

        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {
            $etat = $repoEtat->findOneBy(['libelle' => 'Créée']);
            $ville = $repoVille->findOneBy(['nom' => $request->get('villes')]);
            $lieu = $repoLieu->findOneBy(['nom' => $request->get('lieux')]);
            if (!$lieu) {
                $lieu = new Lieu();
                $lieu->setNom($request->get('lieux'));
                $lieu->setRue($request->request->get('sortie')['rue']);
                $lieu->setLatitude($request->request->get('sortie')['latitude']);
                $lieu->setLongitude($request->request->get('sortie')['longitude']);

                $em->persist($lieu);
                $em->flush();
            }
            $lieu->setVille($ville);

            $sortie->setLieu($lieu);
            $sortie->setOrganisateur($organisateur);
            $sortie->setEtat($etat);

            $sortie->setPublie(false);

            //Insert
            $em->persist($sortie);
            $em->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render('sortie/index.html.twig', [
            'form' => $sortieForm->createView(),
            'villes' => $villes,
            'lieux' => $lieux,
            'site' => $site->getNom(),
            'type' => 'create'
        ]);
    }

    /**
     * @Route("/sortie/lieu/{idLieu}", name="sortie_lieu")
     */
    public function getInfosLieux($idLieu, LieuRepository $repoLieu)
    {
        $lieu = $repoLieu->findOneBy(['id' => $idLieu]);

        $lieu2 = array(
            'nom' => $lieu->getNom(),
            'id' => $lieu->getId(),
            'latitude' => $lieu->getLatitude(),
            'longitude' => $lieu->getLongitude(),
            'code_postal' => $lieu->getVille()->getCodePostal(),
            'rue' => $lieu->getRue()
        );

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode(
            array(
                'result' => true,
                'lieu' => $lieu2
            )
        ));

        return $response;
    }

    /**
     * Add the participant to the designed sortie, the function check the existance of the both elements
     * @Route("/sortie/addParticipant/", name="sortie_add_participant")
     * @param Int $idSortie
     * @param Int $idParticipant
     * @param EntityManagerInterface $em
     * @param ParticipantRepository $repoParticipant
     * @param SortieRepository $repoSortie
     * @return Response
     */
    public function addParticipantToSortie(Request $request, EntityManagerInterface $em, ParticipantRepository $repoParticipant,SortieRepository $repoSortie,UserInterface $user=null) {
        if($user) {
            $sortie = $repoSortie->find($request->get('idSortie'));
            $participant = $repoParticipant->find($request->get('idParticipant'));
            $retour = false;
            if ($sortie && $participant && ($this->getUser()->getId() == $participant->getId() OR in_array('ROLE_ADMIN',$this->getUser()->getRoles()))) {                                                               // if both sortie and participants are set, we gonna check the front already tested conditions another time in back
                if ($sortie->getDateCloture() > date('m/d/Y h:i:s a', time()) && $sortie->getDateDebut() > date('m/d/Y h:i:s a', time()) && $sortie->getEtat() == 'Ouverte' && $sortie->getPublie() == 1) {
                    $sortie->addInscrit($participant);
                    $em->persist($sortie);
                    $em->flush();
                    $retour = true;
                }
            }
        }
        return $this->redirectToRoute("home");
    }
    /**
     * remove the participant to the designed sortie, the function check the existance of the both elements
     * @Route("/sortie/removeParticipant/", name="sortie_remove_participant")
     * @param Int $idSortie
     * @param Int $idParticipant
     * @param EntityManagerInterface $em
     * @param ParticipantRepository $repoParticipant
     * @param SortieRepository $repoSortie
     * @return Response
     */
    public function removeParticipantToSortie(Request $request, EntityManagerInterface $em, ParticipantRepository $repoParticipant,SortieRepository $repoSortie) {
        $sortie = $repoSortie->find($request->get('idSortie'));
        $participant = $repoParticipant->find($request->get('idParticipant'));
        $retour = false;
        if($sortie && $participant) {
            if($sortie->getEtat() == 'Ouverte' && ($this->getUser()->getId() == $participant->getId() OR in_array('ROLE_ADMIN',$this->getUser()->getRoles()))) {
                $sortie->removeInscrit($participant);
                $em->persist($sortie);
                $em->flush();
                $retour = true;
            }
        }
        return $this->redirectToRoute("home");
    }

    /**
     * Display the choosen "sortie" and give to the twig the necessary informations to handle the modification or not
     * @param $id
     * @param SortieRepository $repo
     * @param VilleRepository $repoVille
     * @param LieuRepository $repoLieu
     * @param Request $request
     * @return Response
     * @Route("/sortie/{id}", name="sortie_showOne")
     */
    public function showOne($id, EntityManagerInterface $em, SortieRepository $repo, VilleRepository $repoVille, LieuRepository $repoLieu, ParticipantRepository $repoParticipant, Request $request, EtatRepository $repoEtat) {

        $sortie = $repo->findOneBy(['id' => $id]);
        $user = $this->getUser();
        $organisateur = $repoParticipant->findOneBy(['id' => $user->getId()]);

        $site = $sortie->getOrganisateur()->getSite();
        $villes = $repoVille->findAll();
        $lieux = $repoLieu->findAll();

        $sortieForm = $this->createForm(SortieType::class, $sortie);
        $sortieForm->get('rue')->setData($sortie->getLieu()->getRue());
        $sortieForm->get('latitude')->setData($sortie->getLieu()->getLatitude());
        $sortieForm->get('longitude')->setData($sortie->getLieu()->getLongitude());
        $sortieForm->handleRequest($request);

        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {

            $ville = $repoVille->findOneBy(['nom' => $request->get('villes')]);
            $lieu = $repoLieu->findOneBy(['nom' => $request->get('lieux')]);
            $lieu->setVille($ville);

            $sortie->setLieu($lieu);
            $sortie->setOrganisateur($organisateur);

            //Insert
            $em->persist($sortie);
            $em->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render('sortie/index.html.twig', [
            'form' => $sortieForm->createView(),
            'sortie' => $sortie,
            'site' => $site->getNom(),
            'villes' => $villes,
            'lieux' => $lieux,
            'type' => 'update'
        ]);
    }
    /**
     * @Route("/sortie/publi/", name="sortie_publi")
     */
    public function publierSortie(Request $request, SortieRepository $repo, EntityManagerInterface $em, EtatRepository $repoEtat) {
        $sortie = $repo->findOneBy(['id' => $request->get('id')]);

        if ($sortie && $this->getUser()->getId() == $sortie->getOrganisateur()->getId()) {
            $sortie->setPublie(true);
            $etat = $repoEtat->findOneBy(['libelle' => 'Ouverte']);
            $sortie->setEtat($etat);

            $em->persist($sortie);
            $em->flush();
        }

        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/sortie/annuler/", name="sortie_annuler")
     */
    public function annulerSortie(Request $request, SortieRepository $repo, EntityManagerInterface $em, EtatRepository $repoEtat,UserInterface $user,\Swift_Mailer $mailer) {

        $sortie = $repo->findOneBy(['id' => $request->get('id')]);
        $etat = $repoEtat->findOneBy(['libelle' => 'Annulée']);
        if (in_array('ROLE_ADMIN', $user->getRoles())) {                              // if the current user is an admin, then we shall inform the user that his sortie has been cancelled
            $message = (new \Swift_Message('Au sujet de votre sortie '.$sortie->getNom()))
                ->setFrom('projetbleusortie@gmail.com')
                ->setTo($sortie->getOrganisateur()->getMail())
                ->setBody(
                    $this->renderView('mail/sortieAnnuleByAdmin.html.twig', [
                        'participant' => $sortie->getOrganisateur(),
                        'sortie' => $sortie,
                    ]),
                    'text/html'
                );
            $mailer->send($message);
        }
        if ($sortie && ($this->getUser()->getId() == $sortie->getOrganisateur()->getId() OR in_array('ROLE_ADMIN',$this->getUser()->getRoles()))) {
            $sortie->setEtat($etat);
            $sortie->setMotif($request->get('motif'));
            $em->persist($sortie);
            $em->flush();
        }

        return $this->redirectToRoute("home");
    }
}

<?php

namespace App\Controller;

use App\Entity\Participant;

use App\Entity\Site;
use App\Form\EditParticipantFormType;
use App\Form\ParticipantImportType;

use App\Form\PasswordParticipantFormType;
use App\Form\PasswordRecoverFormType;
use App\Form\RegistrationFormType;
use App\Repository\ParticipantHashRepository;
use App\Utils\ParticipantUtils;
use App\Repository\ParticipantRepository;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
class ParticipantController extends AbstractController
{



    /**
     * @Route("/admin/participant/update/actif/{id}", name="participant_update_actif")
     */
    public function update_actif($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $participant =  $em->getRepository(Participant::class)->find($id);
        $value = $request->get('actif') == "true";
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode(
            array(
                'result'=>false
            )
        ));
        if(!empty($participant)){

            $participant->setActif($value);
            $em->flush();

            $response->setContent(json_encode(
                array(
                    'result'=>true,
                    'actif' => $participant->getActif()
                )
            ));



        }

        return $response;
    }
    /**
     * @Route("/admin/participant/update/admin/{id}", name="participant_update_admin")
     */
    public function update_admin($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $participant =  $em->getRepository(Participant::class)->find($id);
        $value = $request->get('admin') == "true";
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode(
            array(
                'result'=>false
            )
        ));
        if(!empty($participant)){

            $participant->setAdmin($value);
            $em->flush();

            $response->setContent(json_encode(
                array(
                    'result'=>true,
                    'admin' => $participant->getAdmin()
                )
            ));



        }

        return $response;
    }
    /**
     * @Route("/admin/participant/supprimer/{id}", name="participant_delete")
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $participant =  $em->getRepository(Participant::class)->find($id);

        if(!empty($participant)){

            $em->remove($participant);
            $em->flush();

        }
        return$this->redirectToRoute('participant_list');

    }
    /**
     * @Route("/admin/participant/liste", name="participant_list")
     */
    public function list()
    {
        $participants = $this->getDoctrine()->getManager()->getRepository(Participant::class)->findAll();
        return $this->render('participant/list.html.twig', [
            'participants' => $participants,
        ]);
    }
    /**
     * @Route("/admin/participant/nouveau", name="participant_create")
     */
    public function create_participant(\Symfony\Component\HttpFoundation\Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new Participant();
        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
                                                                // password encoding in Bcrypt
            $user->setMotDePasse(                                                                           // by default, a new user is active
                $passwordEncoder->encodePassword(                                                           // but not an administrator
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setActif(true);
            $user->setAdmin(false);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $response = $this->redirectToRoute("participant_list");


        }else{
            $response =  $this->render('participant/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }
       return $response;
    }
    /**
     * @Route("/participant/afficher/{id}", name="participant_show")
     */
    public function show($id,SessionInterface $session, UserInterface $user = null)
    {
        $participant = $this->getDoctrine()->getRepository(Participant::class)->find($id);
        if(!empty($participant)){
            $status = Array('isOwner' => false,'isAdmin' => false);
            if($user) {
               $status = $this->getProfileRights($participant,$user);
            }
            $response =  $this->render('participant/show.html.twig', [
                'participant' => $participant,
                'isOwner' => $status['isOwner'],
                'isAdmin' => $status['isAdmin'],
            ]);
        } else {
            $response = new Response("Not found");
            $response->setStatusCode(404);
        }

        return $response;
    }

    /**
     * @Route("/admin/participant/edit/{id}", name="participant_edit")
     * @param $id
     * @param UserInterface|null $user
     * @param Request $request
     * @return Response
     */
    public function edit($id,UserInterface $user = null,Request $request,ValidatorInterface $validator) {
        $errors=array();
        if(!empty($id)) {
            $participant = $this->getDoctrine()->getRepository(Participant::class)->find($id);
            if(!empty($participant)) {
                $form = $this->createForm(EditParticipantFormType::class, $participant);
                $formPassword = $this->createForm(PasswordparticipantFormType::class, $participant);
                $form->handleRequest($request);
                $formPassword->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()) {                                                      // on classic form completion
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($participant);
                    $entityManager->flush();
                }
                if($formPassword->isSubmitted() && $formPassword->isValid()) {                                     // When the user complete the password change form
                    $status = $this->passwordChangeForParticipant($formPassword->get('plainPassword1')->getData(),$formPassword->get('plainPassword2')->getData(),$participant);
                    if(!$status) {                                                                                             // the new password has not been setted
                        $errors[] = 'Les mots de passe ne correspondent pas';
                    }
                }
                $response = $this->render('participant/edit.html.twig', [                                       // default view
                    'participant' => $participant,
                    'registrationForm' => $form->createView(),
                    'passwordForm' => $formPassword->createView(),
                    'errors' => $errors,
                ]);
            }
        }
        return $response;
    }
    /**
     * @Route("/participant/mon-profil/mdp", name="participant_edit_password")
     * @param $id
     * @param UserInterface|null $user
     * @param Request $request
     * @return Response
     */
    public function edit_password(UserInterface $user = null,Request $request,ValidatorInterface $validator) {
        $errors=array();
        $id = $this->getUser()->getId();
        if(!empty($id)) {
            $participant = $this->getDoctrine()->getRepository(Participant::class)->find($id);
            if(!empty($participant)) {
                $formPassword = $this->createForm(PasswordparticipantFormType::class, $participant);
                $formPassword->handleRequest($request);
                if($formPassword->isSubmitted() && $formPassword->isValid()) {                                     // When the user complete the password change form
                    $status = $this->passwordChangeForParticipant($formPassword->get('plainPassword1')->getData(),$formPassword->get('plainPassword2')->getData(),$participant);
                    if(!$status) {                                                                                             // the new password has not been setted
                        $errors[] = 'Les mots de passe ne correspondent pas';
                    }
                }
                $response = $this->render('participant/edit_password.html.twig', [                                       // default view
                    'participant' => $participant,
                    'passwordForm' => $formPassword->createView(),
                    'errors' => $errors,
                ]);
            }
        }
        return $response;
    }

    /**
     * @Route("/participant/mon-profil/infos", name="participant_edit_mon_pofil")
     * @param $id
     * @param UserInterface|null $user
     * @param Request $request
     * @return Response
     */
    public function edit_mon_profil(UserInterface $user = null,Request $request,ValidatorInterface $validator) {
        $errors=array();
        $id = $this->getUser()->getId();
            $participant = $this->getDoctrine()->getRepository(Participant::class)->find($id);
            if(!empty($participant)) {
                $form = $this->createForm(EditParticipantFormType::class, $participant);
                $form->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()) {                                                      // on classic form completion
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($participant);
                    $entityManager->flush();
                }

                $response = $this->render('participant/edit_infos.html.twig', [                                       // default view
                    'participant' => $participant,
                    'registrationForm' => $form->createView(),
                    'errors' => $errors,
                ]);
            }
        return $response;
    }
    /**
     * @Route("/participant/mon-profil/photo", name="participant_edit_photo")
     * @param $id
     * @param UserInterface|null $user
     * @param Request $request
     * @return Response
     */
    public function edit_photo(UserInterface $user = null,Request $request,ValidatorInterface $validator) {
        $errors=array();
        $id = $this->getUser()->getId();
            $participant = $this->getDoctrine()->getRepository(Participant::class)->find($id);
            if(!empty($participant)) {


                $response = $this->render('participant/edit_photo.html.twig', [                                       // default view
                    'participant' => $participant,
                    'errors' => $errors,
                ]);
            }
        return $response;
    }
    /**
     * @param String $password1
     * @param String $password2
     * @param Participant $participant
     * @return bool
     */
    public function passwordChangeForParticipant(String $password1,String $password2, Participant $participant) :bool {
        if($password1 == $password2) {
            $participant->setMotDePasse(password_hash($password1,PASSWORD_BCRYPT));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($participant);
            $entityManager->flush();
            $response = true;
        } else {
            $response = false;
        }
        return $response;
    }

    /**
     * Retourne les droits d'un utilisateur sur un profil donné
     * @param Participant $participant
     * @param UserInterface $user
     * @return array
     */
    public function getProfileRights(Participant $participant, UserInterface $user) {
        $isAdmin = false;
        $isOwner = false;
        if (in_array('ROLE_ADMIN', $user->getRoles())) {                              // if the current user is an admin, he will get all owning rights
            $isAdmin = true;
        }
        if ($user->getUsername() == $participant->getMail()) {
            $isOwner = true;
        }
        return array('isAdmin' => $isAdmin,'isOwner' => $isOwner);
    }
    /**
     * @Route("/admin/participant/import", name="participant_import")
     */
    public function import(Request $request,ValidatorInterface $validator)
    {

        $form = $this->createForm(ParticipantImportType::class);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $fichier */
            $fichier = $form['fichier']->getData();





            $pAarray = json_decode(file_get_contents($fichier->getPathname()),true);
            $is_json =  (json_last_error() == JSON_ERROR_NONE);
            if(!$is_json){
                $pAarray = null;


                $csv_mime_types = [
                    'text/csv',
                    'text/plain',
                    'application/csv',
                    'text/comma-separated-values',
                    'application/excel',
                    'application/vnd.ms-excel',
                    'application/vnd.msexcel',
                    'text/anytext',
                    'application/octet-stream',
                    'application/txt',
                ];
                $finfo = finfo_open( FILEINFO_MIME_TYPE );
                $mime_type = finfo_file( $finfo, $fichier->getPathname() );

                if(in_array( $mime_type, $csv_mime_types )){
                    $pAarray = ParticipantUtils::csvToArray($fichier->getPathname());
                }

            }


            if (!empty($pAarray)) {

                $validUsers = array();
                $invalidUsers = array();
                foreach ($pAarray as $u) {

                    $tmp = ParticipantUtils::arrayToParticipant($u,$this->getDoctrine()->getManager(),null);
                    $error = $validator->validate($tmp);
                    if (count($error) == 0) {
                        $validUsers[] = $tmp;
                    } else {
                        $invalidUsers[] = $tmp;
                    }
                }

                $response = $this->render('participant/import_confirm.html.twig',
                    array(
                        'valid' => $validUsers,
                        'invalid' => $invalidUsers,
                        "users" => json_encode($pAarray)
                    ));
            }else{
                $response =  $this->render('participant/import.html.twig',array('form'=>$form->createView()));

            }
        }else{

            $response =  $this->render('participant/import.html.twig',array('form'=>$form->createView()));

        }

        return $response;
    }


    /**
     * @Route("/admin/participant/import/validate", name="participant_import_validate")
     */
    public function import_validate(Request $request,ValidatorInterface $validator,UserPasswordEncoderInterface $encoder)
    {
        $users = $request->get('users');
        if(!empty($users)){

            $em = $this->getDoctrine()->getManager();
            $array = json_decode($users,true);
            foreach ($array as $u){
                $tmp = ParticipantUtils::arrayToParticipant($u,$this->getDoctrine()->getManager(),$encoder);
                $error = $validator->validate($tmp);
                if(count($error) == 0){
                    $em->persist($tmp);

                }
            }
            $em->flush();


        }
        return $this->redirectToRoute("participant_list");


    }
    /**
     * @Route("/participant/import/photo", name="participant_upload_photo")
     */
    public function upload_photo(Request $request,ValidatorInterface $validator,UserPasswordEncoderInterface $encoder)
    {
        $user = $this->getUser();
        $uploadDir = $this->getParameter("photo_dir");

        $files = $request->files->get('files');
        if(!empty($files)){
            $f= $files[0];
            $filename = $user->getId().'.'.$f->guessExtension();
            $f->move($uploadDir,$filename);

        $em = $this->getDoctrine()->getManager();
        $user->setPhotoUrl("/uploads/".$filename);
        $em->flush();
            $resp =  new Response("");
        }else{

            $resp =  new Response("",402);
        }

      return $resp;

    }

    /**
     * the user will receive an email to provide him a password change solution
     * @Route("/iForgotMyPassword", name="app_i_forgot_my_password")
     * @throws \Exception
     */
    public function iForgotMyPassword(\Swift_Mailer $mailer,Request $request,ValidatorInterface $validator, ParticipantRepository $participantRepository, ParticipantHashRepository $repoHash) {
        $user = new Participant();
        $form = $this->createForm(PasswordRecoverFormType::class, $user);
        $response =  $this->render('participant/passwordRecover.html.twig', [
            'form' => $form->createView(),
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted()) {                                                                                  // pas de isValid() car l'adresse existe en base, c'est logique
            if($user->getMail()) {
                $email = $user->getMail();
                $emailConstraint = new Assert\Email();

                $emailConstraint->message = 'Veuillez compléter le champ "Email" correctement.';                    // On check si l'email est correct
                $errors = $validator->validate(                                                                  // On ira ensuite regarder si un participant possède cette adresse
                    $email,
                    $emailConstraint
                );
                if(count($errors) == 0) {                                                                        // si mail correct on cherche le participant avec cette adresse
                    $participant = $participantRepository->findOneBy(array('mail' => $email));
                    if ($participant && $participant->getActif()) {
                        $hash = ParticipantUtils::generateParticipantHash($participant,$participantRepository,$repoHash,$this->getDoctrine()->getManager());
                        $message = (new \Swift_Message('Au sujet de votre compte'))
                            ->setFrom('projetbleusortie@gmail.com')
                            ->setTo($participant->getMail())
                            ->setBody(
                                $this->renderView('mail/recoverPasswordMail.html.twig', [
                                    'participant' => $participant,
                                    'hash' => $hash,
                                ]),
                                'text/html'
                            );
                        $mailer->send($message);
                        $retour = 'Un message a été envoyé sur la boite mail <b>'.$participant->getMail(). '</b> afin de rétablir un nouveau mot de passe.';
                        $retour.= '<hr><p class="mb-0">Vous allez automatiquement être redirigé vers l\'accueil.</p>';
                        $response =  $this->render('home/messageBox.html.twig', [
                            'message' => $retour,
                            'type' => 'success',
                        ]);
                    } else {
                        $error = 'Cette adresse ne correspond à aucun participant.';
                        $response =  $this->render('participant/passwordRecover.html.twig', [
                            'form' => $form->createView(),
                            'error' => $error,
                        ]);
                    }
                }
            }
        }

        return $response;
    }


    /**
     * According to the hash, the anonymous user will be able to change the password of the related participant
     * @Route("/regeneratePassword/{hash}", name="app_regeneratePassword")
     */
    public function regeneratePassword(String $hash,ParticipantRepository $participantRepository, ParticipantHashRepository $repoHash, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $participantHash = $repoHash->findOneBy(array('hash'=>$hash));
        $type='success';
        $retour='Une erreur inconnue a eue lieu';
        if($participantHash) {
            $participant = $participantRepository->findOneBy(array('id' => $participantHash->getIdParticipant()));
            if($participant) {
                $formPassword = $this->createForm(PasswordparticipantFormType::class, $participant);
                $formPassword->handleRequest($request);

                if($formPassword->isSubmitted() && $formPassword->isValid()) {
                    $status = $this->passwordChangeForParticipant($formPassword->get('plainPassword1')->getData(),$formPassword->get('plainPassword2')->getData(),$participant);
                    if($status) {
                        $retour = 'La modification de votre mot de passe a été opérée depuis l\'adresse <b>'.$participant->getMail(). '</b> Les changements sont sauvegardés.';
                        $retour.= '<hr><p class="mb-0">Vous allez automatiquement être redirigé vers l\'accueil.</p>';
                        $em->remove($participantHash);
                        $em->flush();
                    }
                }
            } else {
                $retour='L\'utilisateur lié à ce lien a été supprimé.';
                $type='danger';
            }
        } else {
            $retour='Le lien de sauvetage que vous utilisez actuellement n\'est plus valide.';
            $type='danger';
        }
        if(isset($formPassword) AND ((!$formPassword->isSubmitted() AND $participant) OR ($formPassword->isSubmitted() AND !$status))) {
            $error=null;
            if(isset($status) AND !$status) {
                $error = 'Merci de faire correspondre les mots de passe.';
            }
            $response =  $this->render('participant/edit_password.html.twig', [
                'passwordForm' => $formPassword->createView(),
                'participant' => $participant,
                'error' => $error,
            ]);
        } else {
            $response =  $this->render('home/messageBox.html.twig', [
                'message' => $retour,
                'type' => $type,
            ]);
        }
        return $response;
    }


}

<?php

namespace App\Controller;

use App\Entity\Groupe;
use App\Form\GroupeParticipantsType;
use App\Form\GroupeType;
use App\Repository\GroupeRepository;
use App\Repository\ParticipantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class GroupeController extends AbstractController
{
    /**
     * @Route("/groupe", name="groupe")
     * @param GroupeRepository $groupeRepository
     * @param UserInterface $user
     * @param ParticipantRepository $participantRepository
     * @return Response
     */
    public function index(GroupeRepository $groupeRepository,UserInterface $user, ParticipantRepository $participantRepository)
    {
        $user = $participantRepository->findOneBy(Array('mail'=>$user->getUsername()));
        if(!in_array('ROLE_ADMIN',$user->getRoles())) {
            $groupes = $groupeRepository->findBy(Array('proprietaireGroupe' => $user->getId()));
        } else {
            $groupes = $groupeRepository->findAll();
        }

        return $this->render('groupe/index.html.twig', [
            'groupes' => $groupes,
        ]);
    }


    /**
     * @Route("/groupe/ajouter", name="groupe_add")
     * @param GroupeRepository $groupeRepository
     * @param Request $request
     * @return Response
     */
    public function add(GroupeRepository $groupeRepository, Request $request, UserInterface $user, ParticipantRepository $participantRepository)
    {
        $groupe = new Groupe();
        $form = $this->createForm(GroupeType::class,$groupe);
        $form->handleRequest($request);
        $response = $this->render('groupe/create.html.twig', [
        'form' => $form->createView(),
        ]);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $participantRepository->findOneBy(Array('mail'=>$user->getUsername()));
            $groupe->setProprietaireGroupe($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($groupe);
            $entityManager->flush();
            $response = $this->redirectToRoute("groupe");
        }
        return $response;
    }

    /**
     * @Route("/groupe/showone/{id}", name="groupe_showOne")
     */
    public function showOne($id,GroupeRepository $groupeRepository,ParticipantRepository $participantRepository,Request $request, UserInterface $user) {
        $groupe = $groupeRepository->findOneBy(Array('id'=>$id));
        if($groupe->getProprietaireGroupe()->getUsername() == $user->getUsername() OR in_array('ROLE_ADMIN',$user->getRoles())) {
            $lesParticipants = $participantRepository->findAll();
            $entityManager = $this->getDoctrine()->getManager();
            $form = $this->createForm(GroupeParticipantsType::class, $groupe);
            $form->handleRequest($request);
            if ($groupe) {
                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager->persist($groupe);
                    $entityManager->flush();
                    return $this->redirectToRoute("groupe");
                } else {
                    $response = $this->render('groupe/showOne.html.twig', [
                        'participants' => $lesParticipants,
                        'form' => $form->createView()
                    ]);
                }
            }
        } else {
            $response = $this->redirectToRoute("groupe");
        }
        return $response;
    }

    /**
     * @Route("/groupe/delete/{id}", name="groupe_delete")
     */
    public function delete($id,GroupeRepository $groupeRepository, UserInterface $user) {
        $groupe = $groupeRepository->findOneBy(Array('id'=>$id));
        $entityManager = $this->getDoctrine()->getManager();
        if($groupe) {
            if($groupe->getProprietaireGroupe()->getUsername() == $user->getUsername() OR in_array('ROLE_ADMIN',$user->getRoles())) {
                $entityManager->remove($groupe);
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute("groupe");
    }
}

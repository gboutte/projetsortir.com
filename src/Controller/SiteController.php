<?php

namespace App\Controller;

use App\Entity\Site;
use App\Form\PasswordParticipantFormType;
use App\Form\SiteType;
use App\Repository\ParticipantRepository;
use App\Repository\SiteRepository;
use App\Repository\SortieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Tests\RequestStackTest;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    /**
     * @Route("/admin/site", name="site")
     */
    public function index(SiteRepository $repoSite)
    {
        $sites = $repoSite->findAll();
        return $this->render('site/index.html.twig', [
            'sites' => $sites,
        ]);
    }

    /**
     * @Route("/admin/site/ajout/", name="site_ajout")
     */
    public function ajout(Request $request)
    {
        $site = new Site();
        $form = $this->createForm(SiteType::class, $site);
        $form->handleRequest($request);
        $response = $this->render('site/create.html.twig', [
            'form' => $form->createView(),
        ]);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($site);
            $entityManager->flush();
            $response = $this->redirectToRoute('site');
        }
        return $response;
    }

    /**
     * @Route("/admin/site/delete/{id}", name="site_delete")
     *
     */
    public function delete(Int $id,SiteRepository $repoSite,ParticipantRepository $repoParticipant) {
        $site = $repoSite->findOneBy(array('id' => $id));
        $entityManager = $this->getDoctrine()->getManager();
        if($site) {
            $participantLie = $repoParticipant->findOneBy(array('site'=>$id));
            if(!$participantLie) {                                                                                         // See if the site is attached with one or more participants
                $entityManager->remove($site);                                                                             // We will not allow the suppression of the currently used site
                $entityManager->flush();
                $response = $this->redirectToRoute('site');
            } else {
                $error = 'Impossible de supprimer un site utilisé par des participants !';
                $sites = $repoSite->findAll();
                $response =  $this->render('site/index.html.twig', [
                    'sites' => $sites,
                    'error' => $error,
                ]);
            }
        }
        return $response;
    }
}

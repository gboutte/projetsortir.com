<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Form\LieuType;
use App\Repository\VilleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LieuController extends AbstractController
{
    /**
     * @Route("/admin/lieu", name="lieu")
     */
    public function index(EntityManagerInterface $em)
    {
        $lieux = $em->getRepository(Lieu::class)->findBy([],['nom'=>'ASC']);

        return $this->render('lieu/index.html.twig', [
            'lieux' => $lieux,
        ]);
    }

    /**
     * @Route("/admin/lieu/add", name="lieu_add")
     */
    public function add(Request $request, EntityManagerInterface $em, VilleRepository $repoVille)
    {
        $villes = $repoVille->findAll();
        $lieu = new Lieu();

        $form = $this->createForm(LieuType::class, $lieu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $ville = $repoVille->findOneBy(['nom' => $request->get('villes')]);
            $lieu->setVille($ville);
            $em->persist($lieu);
            $em->flush();

            return $this->redirectToRoute("lieu", ["id" => $lieu->getId()]);
        }

        return $this->render("lieu/add.html.twig", [
            "form" => $form->createView(),
            'villes' => $villes,
        ]);
    }

    /**
     * @Route("/admin/lieu/update/{id}", name="lieu_update", requirements={"id":"\d+"})
     */
    public function update(Lieu $lieu, Request $request, EntityManagerInterface $em, VilleRepository $repoVille)
    {
        $villes = $repoVille->findAll();
        $form = $this->createForm(LieuType::class,$lieu);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $ville = $repoVille->findOneBy(['nom' => $request->get('villes')]);
            $lieu->setVille($ville);
            $em->persist($lieu);
            $em->flush();

            $this->addFlash('success', 'Lieu successfully updated!');
            return $this->redirectToRoute('lieu');
        }

        return $this->render("lieu/add.html.twig", [
            "form" => $form->createView(),
            'villes' => $villes,
            'lieu' => $lieu,
        ]);
    }

    /**
     * @Route("/admin/lieu/del/{id}", name="lieu_del")
     */
    public function del(Lieu $lieu, EntityManagerInterface $em){
        $em->remove($lieu);
        $em->flush();
        $this->addFlash("success", "Lieu successfully deleted!");

        return $this->redirectToRoute('lieu');
    }
}

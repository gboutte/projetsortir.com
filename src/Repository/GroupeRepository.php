<?php

namespace App\Repository;

use App\Entity\Groupe;
use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Groupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Groupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Groupe[]    findAll()
 * @method Groupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Groupe::class);
    }

    // /**
    //  * @return Groupe[] Returns an array of Groupe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param EntityManager $em
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByParticipantsGroupe(EntityManager $em, ParticipantRepository $participantRepository,$user)
    {

        if ($user && $user->getUsername()) {
            $participant = $participantRepository->findOneBy(Array('mail'=>$user->getUsername()));
            if($participant) {
                return $this->createQueryBuilder('g')
                    ->join('g.participants','p')
                    ->andWhere('p.id IN (:val)')
                    ->setParameter('val', $participant->getId())
                    ->orWhere('g.proprietaireGroupe IN (:master)')
                    ->setParameter('master', $participant->getId())
                    ->getQuery()
                    ->getArrayResult();
            }
        }
    }
}

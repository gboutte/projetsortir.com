<?php

namespace App\Repository;

use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    public function findByFilterFields(array $filtres, ParticipantRepository $repo)
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.organisateur', 'p')
            ->leftjoin('s.inscrits', 'i')
            ->join('s.etat', 'e');
        if ($filtres['site'] != "") {
            $qb->andWhere('p.site = :siteId')
                ->setParameter('siteId', $filtres['site']);
        }
        if ($filtres['sortie'] != "") {
            $qb->andWhere('s.nom LIKE :nomSortie')
                ->setParameter('nomSortie', '%'.$filtres['sortie'].'%');
        }
        $datedebut = \DateTime::createFromFormat("d/m/Y H:i",$filtres['deb']);
        if ($filtres['deb'] != "" && $datedebut instanceof \DateTime) {
            $qb->andWhere('s.date_debut >= :deb')
                ->setParameter('deb', $datedebut, \Doctrine\DBAL\Types\Type::DATETIME);
        } else {
            $qb->andWhere('DATE_DIFF(CURRENT_TIME(), s.date_debut) < 30');
        }
        $datefin =\DateTime::createFromFormat("d/m/Y H:i",$filtres['fin']);
        if ($filtres['fin'] != "" && $datefin instanceof \DateTime) {
            $qb->andWhere('s.date_debut <= :fin')
                ->setParameter('fin',$datefin , \Doctrine\DBAL\Types\Type::DATETIME);
        }
        if ($filtres['organisateur'] == "on") {
            $qb->andWhere('s.organisateur = :organisateur')
                ->setParameter('organisateur', $filtres['id']);
        }
        if ($filtres['inscrit'] == "on") {
            $qb->andWhere('i.id = :inscrit')
                ->setParameter('inscrit', $filtres['id']);
        }
        if ($filtres['notInscrit'] == "on") {
            $list = $qb2 = $this->createQueryBuilder('s')
                ->select('DISTINCT s.id')
                ->join('s.inscrits', 'i')
                ->andWhere('i.id = :idParticipant')
                ->setParameter('idParticipant', $filtres['id'])
                ->getQuery()
                ->getResult();
            if (!empty($list)) {
                $qb->andWhere('s.id NOT IN (:listId)')
                    ->setParameter('listId', $list);
            }
        }
        if ($filtres['passee'] == "on") {
            $qb->andWhere('e.libelle = \'passee\'');
        }
        if (!empty($filtres['groupeOrganisateur'])) {
            $qb->andWhere('s.groupeOrganisateur IN (:groupes) OR s.groupeOrganisateur IS NULL ')
                ->setParameter('groupes', $filtres['groupeOrganisateur']);
        } else {
            $qb->andWhere('s.groupeOrganisateur IS NULL ');
        }
        $lis = $qb->getQuery()
            ->getResult();

        return $lis;
    }


    /*
    public function findOneBySomeField($value): ?Sortie
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
